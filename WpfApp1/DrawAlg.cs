﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class DrawAlg
    {
        int[,] image;
        int imageHeight;
        int imageWidth;

        public DrawAlg(int [,] image)
        {
            this.image = image;
            imageHeight = image.GetLength(0);
            imageWidth = image.GetLength(1);
        }

        public List<Command> GetCommands()
        {
            List<Command> commands = new List<Command>();

            commands.Add(new Command() { CommandType = CommandType.PenDown });
            commands.Add(new Command() { CommandType = CommandType.PenUp });

            RobotState robotState = new RobotState()
            {
                Angle = 0,
                IsPenDown = false,
                Point = new Point() {Col = 0, Row = 0}
            };

            NextPoint nextPoint = new NextPoint();

            while ((nextPoint = GetNearestPoint(robotState)).Point != null)
            {
                if (!robotState.IsPenDown && nextPoint.IsNeighbor && robotState.Point.Col != 0 && robotState.Point.Row != 0)
                {
                    commands.Add(new Command() { CommandType = CommandType.PenDown });
                    robotState.IsPenDown = true;
                }
                else if (!nextPoint.IsNeighbor)
                {
                    commands.Add(new Command() { CommandType = CommandType.PenUp });
                    robotState.IsPenDown = false;
                }

                Movement movement = GetMovementToPoint(robotState, nextPoint.Point);

                double angletoTurn = GetSmallestAngleToTurn(robotState, movement);

                if (Math.Abs(angletoTurn) > 0)
                {
                        commands.Add(new Command()
                        {
                            CommandType = ((angletoTurn > 0) ? CommandType.Left : CommandType.Right),
                            Value = Math.Abs(angletoTurn)
                        });

                        robotState.Angle = movement.Angle;
                }

                if (movement.Distance != 0)
                {
                    commands.Add(new Command()
                    {
                        CommandType = CommandType.Forward,
                        Value = Math.Abs(movement.Distance)
                    });

                    robotState.Point.Col = nextPoint.Point.Col;
                    robotState.Point.Row = nextPoint.Point.Row;
                }

                image[nextPoint.Point.Row, nextPoint.Point.Col] = 0;
            }

            commands.Add(new Command() { CommandType = CommandType.PenUp });

            return commands;
        }

        private NextPoint GetNearestPoint(RobotState robotState)
        {
            NextPoint nextPoint = new NextPoint
            {
                IsNeighbor = false
            };

            for (int searchSquareSize = 1; searchSquareSize < imageWidth - 1; searchSquareSize++)
            {
                Point point = null;

                if (searchSquareSize == 1)
                {
                    nextPoint.IsNeighbor = true;
                }
                else
                {
                    nextPoint.IsNeighbor = false;
                }

                if ((robotState.Point.Row - searchSquareSize < imageHeight)
                        && (robotState.Point.Row - searchSquareSize >= 0)
                        && (image[robotState.Point.Row - searchSquareSize, robotState.Point.Col] == 1))
                {
                    point = new Point
                    {
                        Row = robotState.Point.Row - searchSquareSize,
                        Col = robotState.Point.Col
                    };
                }
                else if ((robotState.Point.Col + searchSquareSize < imageWidth) && (image[robotState.Point.Row, robotState.Point.Col + searchSquareSize] == 1))
                {
                    point = new Point
                    {
                        Row = robotState.Point.Row,
                        Col = robotState.Point.Col + searchSquareSize
                    };
                }
                else if ((robotState.Point.Row + searchSquareSize < imageHeight) && (image[robotState.Point.Row + searchSquareSize, robotState.Point.Col] == 1))
                {
                    point = new Point
                    {
                        Row = robotState.Point.Row + searchSquareSize,
                        Col = robotState.Point.Col
                    };
                }
                else if ((robotState.Point.Col - searchSquareSize < imageWidth)
                    && (robotState.Point.Col - searchSquareSize >= 0)                    
                    && image[robotState.Point.Row, robotState.Point.Col - searchSquareSize] == 1)
                {
                    point = new Point
                    {
                        Row = robotState.Point.Row,
                        Col = robotState.Point.Col - searchSquareSize
                    };
                }

                if (point != null && point.Row >= 0 && point.Col >= 0 && point.Row < imageHeight && point.Col < imageWidth)
                {
                    nextPoint.Point = point;
                    return nextPoint;
                }
                else
                {
                    for (int i = robotState.Point.Row - searchSquareSize; i <= robotState.Point.Row + searchSquareSize; i++)
                    {
                        for (int j = robotState.Point.Col - searchSquareSize; j <= robotState.Point.Col + searchSquareSize; j++)
                        {
                            if ((i < 0) || (j < 0) || (i >= imageHeight) || (j >= imageWidth)) continue;
                            if (i == robotState.Point.Row && j == robotState.Point.Col) continue;
                            if (image[i, j] != 0)
                            {
                                point = new Point
                                {
                                    Row = i,
                                    Col = j
                                };
                                nextPoint.Point = point;
                                return nextPoint;
                            }
                        }
                    }
                }
            }

            return nextPoint;
        }

        private Movement GetMovementToPoint(RobotState robotState, Point point)
        {
            Movement movement = new Movement() { Angle = 0, Distance = 0 };
            if (point.Col > robotState.Point.Col && point.Row == robotState.Point.Row)
            {
                movement.Angle = 0;
                movement.Distance = (double) (point.Col - robotState.Point.Col);
            }
            else if (point.Col > robotState.Point.Col && point.Row < robotState.Point.Row)
            {
                double a = (double)(robotState.Point.Row - point.Row);
                double b = (double)(point.Col - robotState.Point.Col);
                movement.Angle = RadianToDegree(Math.Atan(a / b));
                movement.Distance = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            }
            else if (point.Col == robotState.Point.Col && point.Row < robotState.Point.Row)
            {
                movement.Angle = 90;
                movement.Distance = (double)(robotState.Point.Row - point.Row);
            }
            else if (point.Col < robotState.Point.Col && point.Row < robotState.Point.Row)
            {
                double a = (double)(robotState.Point.Row - point.Row);
                double b = (double)(robotState.Point.Col - point.Col);
                movement.Angle = 180 - RadianToDegree(Math.Atan(a / b));
                movement.Distance = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            }
            else if (point.Col < robotState.Point.Col && point.Row == robotState.Point.Row)
            {
                movement.Angle = 180;
                movement.Distance = (double)(robotState.Point.Col - point.Col);
            }
            else if (point.Col < robotState.Point.Col && point.Row > robotState.Point.Row)
            {
                double a = (double)(robotState.Point.Col - point.Col);
                double b = (double)(point.Row - robotState.Point.Row);
                movement.Angle = 270 - RadianToDegree(Math.Atan(a / b));
                movement.Distance = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            }
            else if (point.Col == robotState.Point.Col && point.Row > robotState.Point.Row)
            {
                movement.Angle = 270;
                movement.Distance = (double)(point.Row - robotState.Point.Row);
            }
            else if (point.Col > robotState.Point.Col && point.Row > robotState.Point.Row)
            {
                double a = (double)(point.Row - robotState.Point.Row);
                double b = (double)(point.Col - robotState.Point.Col);
                movement.Angle = 360 - RadianToDegree(Math.Atan(a / b));
                movement.Distance = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            }
            return movement;
        }

        private double GetSmallestAngleToTurn(RobotState robotState, Movement movement)
        {
            double alpha = movement.Angle - robotState.Angle;
            double beta = movement.Angle - robotState.Angle + 360;
            double gamma = movement.Angle - robotState.Angle - 360;
            if (Math.Abs(alpha) <= Math.Abs(beta) && Math.Abs(alpha) <= Math.Abs(gamma))
            {
                return alpha;
            }
            else if (Math.Abs(beta) <= Math.Abs(alpha) && Math.Abs(beta) <= Math.Abs(gamma))
            {
                return beta;
            }
            else
            {
                return gamma;
            }
        }

        private double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }
    }
}
