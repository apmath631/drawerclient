﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Bitmap imageFromFile;

        string address = "192.168.0.109";
        int port = 5000;

        double distanceFactor = 2.4;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void DrawMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (imageFromFile == null)
            {
                MessageBox.Show("Изображение не загружено");
            }
            else
            {
                var imageAsArray = GetImageAsArray();

                DrawAlg drawAlg = new DrawAlg(imageAsArray);

                var commands = drawAlg.GetCommands();

                var client = new Client(address, port);

                foreach (var command in commands)
                {
                    if (command.CommandType == CommandType.Forward)
                    {
                        client.SendCommand($"fwd:{(command.Value * distanceFactor).ToString().Replace(',', '.')};");
                    }
                    if (command.CommandType == CommandType.Left)
                    {
                        client.SendCommand($"left:{command.Value.ToString().Replace(',', '.')};");
                    }
                    if (command.CommandType == CommandType.Right)
                    {
                        client.SendCommand($"right:{command.Value.ToString().Replace(',', '.')};");
                    }
                    if (command.CommandType == CommandType.PenDown)
                    {
                        client.SendCommand($"penDown;");
                    }
                    if (command.CommandType == CommandType.PenUp)
                    {
                        client.SendCommand($"penUp;");
                    }
                }
                MessageBox.Show("Рисование изображено");
            }
        }

        private int[,] GetImageAsArray()
        {
            Bitmap image = BitmapHelper.Crop(imageFromFile);
            var imageAsArray = BitmapHelper.BitmapToArray(image);        
            return imageAsArray;
        }

        private void LoadImageFromFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "*.*|*.*";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imageFromFile = new Bitmap(openFileDialog.FileName);
                    ImageSource imageSource = new BitmapImage(new Uri(openFileDialog.FileName));
                    ImageControl.Source = imageSource;
                }
                catch(Exception)
                {
                    MessageBox.Show("Файл не является изображением");
                }
            }
        }

        private void ShowAsArray_Click(object sender, RoutedEventArgs e)
        {
            if (imageFromFile == null)
            {
                MessageBox.Show("Изображение не загружено");
            }
            else
            {
                var imageAsArray = GetImageAsArray();
                string output = string.Empty;
                for (int row = 0; row < imageAsArray.GetLength(0); row++)
                {
                    for (int col = 0; col < imageAsArray.GetLength(1); col++)
                    {
                        output += imageAsArray[row, col].ToString();
                    }
                    output += System.Environment.NewLine;
                }
                var memoMessage = new MemoMessage(output);
                memoMessage.ShowDialog();
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            ClearImage();
        }

        public void ClearImage()
        {
            ImageControl.Source = null;
            imageFromFile = null;
        }
    }
}
