﻿namespace WpfApp1
{
    public class Movement
    {
        public double Angle { get; set; }
        public double Distance { get; set; }
    }
}