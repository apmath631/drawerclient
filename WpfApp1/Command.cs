﻿namespace WpfApp1
{
    public class Command
    {
        public CommandType CommandType { get; set; }
        public double Value { get; set; }
    }
}