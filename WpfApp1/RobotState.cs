﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class RobotState
    {
        public bool IsPenDown { get; set; }
        public Point Point { get; set; }
        public double Angle { get; set; }
    }
}
