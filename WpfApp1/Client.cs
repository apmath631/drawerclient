﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    class Client
    {
        string address;
        int port;

        public Client(string address, int port)
        {
            this.address = address;
            this.port = port;
        }

        public void SendCommand(string command)
        {
            TcpClient tcpClient = new TcpClient(address, port);
            NetworkStream tcpStream = tcpClient.GetStream();

            if (!tcpClient.Connected)
            {
                throw new Exception("not connected");
            }

            byte[] sendBytes = Encoding.ASCII.GetBytes(command);
            tcpStream.Write(sendBytes, 0, sendBytes.Length);

            byte[] recievebytes = new byte[tcpClient.ReceiveBufferSize];
            int bytesRead = tcpStream.Read(recievebytes, 0, tcpClient.ReceiveBufferSize);

            string returnData = System.Text.Encoding.ASCII.GetString(recievebytes, 0, recievebytes.Length);

            tcpStream.Close();
            tcpClient.Close();
        }
    }
}
